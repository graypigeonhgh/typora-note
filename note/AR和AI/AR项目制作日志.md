# AR项目制作日志

> AR技术主要涉及有图片检测、平面检测、人脸检测、模型检测、动作捕捉、实时光照反射等。

### 市面主流AR技术

1. ARCore：谷歌开发（面向安卓）、免费（在官网支持的设备列表内的机型才支持）

2. ARKit：苹果公司开发（面向iOS）、免费

3. SenseAR：商汤科技、免费。优点：云锚点免费、手指动作捕捉、满足其最低硬件要求就可实现人脸识别。缺点：只支持安卓系统、技术没有ARCore等成熟。
4. 华为AR：官方文档较为粗糙，开发较为复杂
5. Vuforia：工业级AR技术、收费。优点：图片识别和模型识别技术领先。缺点：贵，平面识别能力较弱。
6. EasyAR：视+AR、有免费版和收费版。优点：云锚点功能较好、大屏AR应用、小程序AR、AR导航功能较为完善。
7. Void AR：太虚AR、收费。优点：Slam效果领先。

（以下为webAR开发平台）

8. AR.js：不推荐，webAR程序兼容性较差。

9. KIVICube：弥知科技。缺点：不能够改域名。优点：小程序AR技术领先

ARFoundation：（unity）AR开发工具集：封装了ARKit和ARCore。

### 一、AR.js

数据一直在加载，失败

官网如下：https://ar-js-org.github.io/AR.js-Docs/



### 二、EasyAR

尝试EasyAR的小程序Demo，效果还行

Demo代码网址：https://github.com/zi-kang/EasyAR-miniprogram-WebAR-Demo

官网开发文档：https://www.easyar.cn/view/login.html



### 三、Unity

成功...