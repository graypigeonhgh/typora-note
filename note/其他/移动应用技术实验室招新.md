# 移动应用技术实验室招新

## 一、简介

移动应用技术实验室隶属于电信学院大创中心，位于主教C1001-C1002，是唯一一间以软件方向为主的实验室，主要从事软件学习与开发工作。我们实验室主要的方向有前端、后端、算法、嵌入式等，同时还涉及到AI、AR、数据分析、ROS、UI设计、爬虫等多项技术。目前，实验室有多位指导老师，曾与校外多家企业和当地政府合作，将专业技术融入实践中。致力于培养具有高创新能力，技术过硬的人才。

## 二、招新方向

前端、后端、算法（限招）、嵌入式（限招）



## 三、收获

1.提供了与学校、老师和外部项目接触的机会，能够通过自主的课外学习应用所掌握的知识来参与项目并获得报酬。

2.参加各项比赛，，在竞争中也能够提升自己。

3.。。。。。。

## 四、招新对象

电子信息工程学院23、24级全体学生。

## 五、招新要求

1.23级学生需要有一定的基础

2.有自主学习能力

3.具有良好的沟通能力和团队合作意识

4.对编程或硬件学习有浓厚的兴趣

## 六、获得奖项

移动应用技术实验室自成立以来，在计算机设计大赛、蓝桥杯、挑战杯、中国高校计算机大赛等比赛中成绩斐然。以下仅展示2024上半学年部分奖项：

蓝桥杯省赛一等奖、计算机设计大赛省赛二等奖、软著（包括在申请）6项、。。。。

## 七、社会实践

三下乡

## 八、常见问题

。。。。。。