### Python后端学习路线

#### 1. MySQL数据库（时限15天）

[MySQL数据库入门到大牛，mysql安装到优化，百科全书级，全网天花板](https://www.bilibili.com/video/BV1iq4y1u7vj/?vd_source=bae65d6b3fd9eccb242aa335de5224d1)

视频从P1看到P95就够了，MySQL建议下载8.0版本的。Python使用框架开发可能可以不用编写sql代码，但还是要懂sql语言和表的设计。

#### 2. Django（时限30天）

[最新Python的web开发全家桶（django+前端+数据库）](https://www.bilibili.com/video/BV1rT4y1v7uQ/?vd_source=76a6d9bc48408247e27bb6b2ff6264d1)

里面讲到的MySQL可以跳过，前端技术需要看得懂基本的流程，可以熟练使用Django编写接口。

#### 3. **Linux**（时限7天）

[3天搞定Linux，1天搞定Shell，清华学神带你通关](https://www.bilibili.com/video/BV1WY4y1H7d3?p=66)

Linux基本学会些常见命令就好了，不需要去背命令！因为Linux的命令有很多很多，在实际开发过程中不会使用到太多种类的命令，即使要用到了只要知道得使用什么样的命令去做什么事情，然后去csdn查找具体的命令是什么就行。

#### 4. Git（时限2天）

[Git最新教程通俗易懂](https://www.bilibili.com/video/BV1FE411P7B3?p=2&vd_source=76a6d9bc48408247e27bb6b2ff6264d1)

#### 5. Redis（时限5天）

[3天搞定Linux，1天搞定Shell，清华学神带你通关](https://www.bilibili.com/video/BV1WY4y1H7d3?p=76)

在项目中，Redis可能不一定要使用到，但后续还是得学的，不如顺便学习。

#### 6. 项目实战

可以自己找一些实战项目练手，因为实在找不到比较好的实战项目就不推荐了

#### 7.项目部署

Python的项目部署我没有尝试过，大体上都是使用（Docker）、Nginx、uwsgi等技术或工具，可自行去查看**Python的web项目部署到云端**的视频。



##### 这些是后端需要掌握的一些基本知识，Python后端后面的知识就需要自己摸索了，后面还涉及到中间件，安全防护，设计模式，消息队列以及Python的其他框架如Flask等等，可以在后面的项目实战中再不断完善自己的技术栈。因为Python后端这块我们实验室并没有师兄学，所有这条路后面就得自己去摸索了。