# maven打包分模块开发项目时报错总结

### 一、Unable to find a single main class from the following candidates

在打包install公共模块的时候，打包失败并报错：

![image-20240415152912454](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240415152912454.png)

**原因：在maven编译这个工程模块的时候，尝试去模块下找main方法的class文件，但是我这个是一个公共服务模块，不需要启动项目，索引不需要main方法文件。**

**解决：在该工程模块的pom.xml中，添加下面的配置进行过滤即可即可**

```java
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
         </plugins>
    </build>
```

### 二、程序包 xxx不存在

**原因：SpringBoot工程打包编译时，会生成两种jar包，一种是普通的jar，另一种是可执行jar。默认情况下，这两种jar的名称相同，在不做配置的情况下，普通的jar先生成，可执行jar后生成，造成可执行jar会覆盖普通的jar。而service工程无法依赖common-utils工程的可执行jar，所以编译失败：程序包不存在。**

**解决：在common-utils工程中，pom.xml文件中加入配置即可：**

```java
<build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <classifier>exec</classifier>
                </configuration>
            </plugin>

        </plugins>
 </build>

```



### 三、其他打包方式

#### 1. 项目打包时会将java目录中的*.xml文件也进行打包 

```java
    <!-- 项目打包时会将java目录中的*.xml文件也进行打包 -->
    <build>
        <plugins>
            <plugin>
                <!--该插件主要用途：构建可执行的JAR -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>

            <!--设置自己目录下的配置文件-->
            <resource>
                <!--下方resources的文件夹名字要和自己项目的文件夹名确认一致才行 很多人就是忽略了名字不一致 -->
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*</include>
                </includes>
            </resource>
        </resources>
    </build>
```

#### 2. 普通打包

```java
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                        </exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
```



> 参考：https://blog.csdn.net/weixin_45108959/article/details/128250343