# SpringBoot项目本地运行正常，jar包运行时前端报错403：No mapping for...

> 提示：在部署jar包到云服务器上之前，一定要在本地运行jar包，查看前端代码是否运行正常，若报错的话可以节省很多时间
>
> 方式：在jar包目录下运行cmd，执行命令：java -jar xxx.jar

主要报错如下： No mapping for GET /tcm/user-appointment/page

![image-20240415161356130](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240415161356130.png)

**本人报错场景** ：本人项目为maven多模块开发，并且在一个含有启动类的maven工程a中导入另一个含有启动类的maven工程b，打包a的jar包并运行时没有报错，但前端调用b里面的接口时报如上错误。

**原因：SpringBoot工程打包编译时，会生成两种jar包，一种是普通的jar，另一种是可执行jar。默认情况下，这两种jar的名称相同，在不做配置的情况下，普通的jar先生成，可执行jar后生成。而本人1工程和2工程的可执行jar，若要在a工程下导入b工程，则需将b工程打包为普通jar包**

**解决：在b工程中，pom.xml文件中加入配置即可：**

```java
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
         </plugins>
    </build>
```

