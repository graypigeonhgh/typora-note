# SpringBoot运行报错：Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException

**具体报错如下：**

![image-20240320153156485](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240320153156485.png)

**原因和解决办法：** 在Mapper中没有添加@Mapper注解，或者在启动类上没有添加@MapperScan(“mapper包的全类名”)注解，导致mapper并没有交给Spring来管理。选择以上一种方法解决即可。