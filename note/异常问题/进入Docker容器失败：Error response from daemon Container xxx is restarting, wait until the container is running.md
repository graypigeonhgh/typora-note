# 进入Docker容器失败：Error response from daemon: Container xxx is restarting, wait until the container is running

### 1.过程

通过jdk17部署jdk17容器

```bash
docker run --restart=always --name jdk17 jdk17
```

部署成功，进入容器报错

```bash
docker exec -it jdk17 /bin/bash
```

具体报错如下：

![image-20240705145253486](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240705145253486.png)

### 2.原因

容器部署时未加上参数：“-it"，使得不能进入容器终端。

> `-it` 是 Docker 命令中的两个参数的组合，其中 `-i` 表示以交互模式运行容器，`-t` 表示为容器分配一个伪终端（pseudo-tty）。这两个参数一起使用可以使得在容器内部进行交互式操作，就像在本地终端中一样。

### 3.解决

```bash
docker run -it --restart=always --name jdk17 jdk17
```

![image-20240705145615043](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240705145615043.png)