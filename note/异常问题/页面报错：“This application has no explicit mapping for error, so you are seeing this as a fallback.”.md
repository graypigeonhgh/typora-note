# 页面报错：“This application has no explicit mapping for /error, so you are seeing this as a fallback.”

<br>

#### 1.具体报错如下：

![image-20231117225540266](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231117225540266.png)

#### 2.可能原因有：

* Application启动类的位置不对，要将Application类放在最外侧，即包含所有子包
* Controller的url路径错误
* 未添加spring-boot-starter-thymeleaf的jar包或yml文件中关于视图解析器的配置问题
* 未开启静态资源映射



> 参考：https://blog.csdn.net/weixin_44299027/article/details/93380344

<br>

#### 3.本人问题：未开启静态资源映射

#### 4.解决办法：配置如下配置类

```java
@Configuration
@Slf4j
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("静态资源映射...");
       
    }
    
}
```