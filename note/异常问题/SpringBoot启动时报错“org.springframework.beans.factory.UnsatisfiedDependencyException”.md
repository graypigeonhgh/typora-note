# 代码生成器生成的项目启动时报错“org.springframework.beans.factory.UnsatisfiedDependencyException”

#### 1. 第一种具体报错如下：

```java
Error starting ApplicationContext. To display the condition evaluation report re-run your application with 'debug' enabled.
2023-11-10T22:07:58.381+08:00 ERROR 34984 --- [           main] o.s.boot.SpringApplication               : Application run failed

org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'answerLikesServiceImpl': Unsatisfied dependency expressed through field 'baseMapper': No qualifying bean of type 'com.hgh.yuzhan.mapper.AnswerLikesMapper' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
	at org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor$AutowiredFieldElement.resolveFieldValue(AutowiredAnnotationBeanPostProcessor.java:767) ~[spring-beans-6.0.13.jar:6.0.13]
......
```



#####  原因：

* 由“No qualifying bean of type 'com.hgh.yuzhan.mapper.AnswerLikesMapper'”可知是service层、Mapper层注解未添加，导致Bean不能定义。

* 代码生成器并没有自动生成@Mapper、@Service注解。

##### 解决

> 在strategyConfig中加入enableMapperAnnotation()   ，开启 @Mapper 注解，重新生成代码后解决
>
> 或者手动添加

#### 2.第二种报错具体如下：

```java
Error starting ApplicationContext. To display the condition evaluation report re-run your application with 'debug' enabled.
2024-03-26T15:47:12.661+08:00 ERROR 99668 --- [           main] o.s.boot.SpringApplication               : Application run failed

org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'answerController': Unsatisfied dependency expressed through field 'answersService': Error creating bean with name 'answerServiceImpl': Unsatisfied dependency expressed through field 'baseMapper': Error creating bean with name 'answerMapper' defined in file [D:\java_code\Git-Repository\forum-parent\forum-front\target\classes\com\hgh\model\mapper\AnswerMapper.class]: Unsatisfied dependency expressed through bean property 'sqlSessionFactory': Error creating bean with name 'sqlSessionFactory' defined in class path resource [com/baomidou/mybatisplus/autoconfigure/MybatisPlusAutoConfiguration.class]: Failed to instantiate [org.apache.ibatis.session.SqlSessionFactory]: Factory method 'sqlSessionFactory' threw exception with message: Failed to parse mapping resource: 'file [D:\java_code\Git-Repository\forum-parent\forum-front\target\classes\mapper\TagsMapper.xml]'
	at org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor$AutowiredFieldElement.resolveFieldValue(AutowiredAnnotationBeanPostProcessor.java:767) ~[spring-beans-6.0.13.jar:6.0.13]
......
```

##### 原因：错误创建bean，可能是注解或者配置上的问题

##### 解决：

* 通过maven工具依次执行clean和install。通过下面的plugins也行。然后通过重新打包，生成新的classes文件。
* 确认@Service和@Mapper注解是否添加

#### 也有可能是entity类或Mapper.xml类出错，总之，具体情况具体分析，对报错信息从下往上分析