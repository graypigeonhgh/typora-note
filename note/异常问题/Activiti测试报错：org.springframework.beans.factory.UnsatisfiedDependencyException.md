## Spring boot 3.x Activiti7 启动失败，并且在一开始导入依赖时无法自动创建表（看到最后）

##### 使用springboot3.x + Activiti7后，发现工作流模块无法启动，并且在一开始导入依赖时无法自动创建表。

### 1. 具体报错如下：

```java
org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'com.hgh.auth.activiti.ProcessTest': Unsatisfied dependency expressed through field 'repositoryService': No qualifying bean of type 'org.activiti.engine.RepositoryService' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations: {@org.springframework.beans.factory.annotation.Autowired(required=true)}
	at org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor$AutowiredFieldElement.resolveFieldValue(AutowiredAnnotationBeanPostProcessor.java:767) ~[spring-beans-6.0.13.jar:6.0.13]
	at org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor$AutowiredFieldElement.inject(AutowiredAnnotationBeanPostProcessor.java:747) ~[spring-beans-6.0.13.jar:6.0.13]
	at org.springframework.beans.factory.annotation.InjectionMetadata.inject(InjectionMetadata.java:145) ~[spring-beans-6.0.13.jar:6.0.13]
	at org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor.postProcessProperties(AutowiredAnnotationBeanPostProcessor.java:492) ~[spring-beans-6.0.13.jar:6.0.13]
......
```

### 2. 运行代码如下：

```java
@SpringBootTest
public class ProcessTest {

    //注入RepositoryService
    @Autowired
    private RepositoryService repositoryService;
    
    //单个文件部署
    @Test
    public void deployProcess() {
        //流程部署
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("process/qingjia.bpmn20.xml")
                .addClasspathResource("process/qingjia.png")
                .name("请假申请流程001")
                .deploy();
        System.out.println(deploy.getId());
        System.out.println(deploy.getName());
    }
}
```

### 3. 报错原因:

这个错误表明Spring框架在尝试创建ProcessTest类的一个bean实例时，无法找到可以注入RepositoryService类型的bean这通常是因为在你的Spring配置中没有定义RepositoryService类型的bean，或者在类没有被Spring框架管理。即流程引擎相关类启动时无法正常注入。

### 4. 解决办法：

#### 方法1

我们需要给RepositoryService注入Bean，创建一个Activiti配置类

```java
/**
 * desc Activiti配置类，为RepositoryService注入Bean
 * @author GreyPigeon mail:2371849349@qq.com
 * @since 2024-04-03-9:25
 **/
@Configuration
public class ActivitiConfig {

    @Bean
    public RepositoryService repositoryService() {
        return ProcessEngines.getDefaultProcessEngine().getRepositoryService();
    }

    @Bean
    public RuntimeService runtimeService() {
        return ProcessEngines.getDefaultProcessEngine().getRuntimeService();
    }

    @Bean
    public TaskService taskService() {
        return ProcessEngines.getDefaultProcessEngine().getTaskService();
    }

}
```

这就给这三个函数注入了bean

在测试类上加上这个注解@SpringBootTest(classes = ActivitiConfig.class) 告诉Spring Boot在运行测试时需要考虑ActivitiConfig类中的配置这样，Spring就能找到并注入需要的bean了

`结果：失败`

#### 方法二：

对于mysql-connector-java版本 ≥ 8.x，需在数据库访问的地址上添加：nullCatalogMeansCurrent=true

如jdbc:mysql://localhost:3306/activiti?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&nullCatalogMeansCurrent=true


> 参考：https://blog.csdn.net/qq_41883461/article/details/134518576

`结果：失败`



### 最后发现：

是因为 springboot3 自动配置注册机制中，自动配置注册的`spring.factories`写法已废弃，已改为`META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports`

#### 方法三：

在resources下新建`META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports`

![image-20240418170948928](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240418170948928.png)

imports文件内填入以下内容，然后重启服务。

```java
org.activiti.common.util.conf.ActivitiCoreCommonUtilAutoConfiguration
org.activiti.application.conf.ApplicationAutoConfiguration
org.activiti.core.common.spring.connector.autoconfigure.ConnectorAutoConfiguration
org.activiti.core.common.spring.identity.config.ActivitiSpringIdentityAutoConfiguration
org.activiti.core.common.spring.project.conf.ProjectModelAutoConfiguration
org.activiti.spring.resources.conf.ResourceFinderAutoConfiguration
org.activiti.core.common.spring.security.config.ActivitiSpringSecurityAutoConfiguration
org.activiti.core.common.spring.security.policies.config.ActivitiSpringSecurityPoliciesAutoConfiguration
org.activiti.api.runtime.conf.impl.CommonModelAutoConfiguration
org.activiti.api.runtime.conf.impl.ProcessModelAutoConfiguration
org.activiti.runtime.api.conf.ConnectorsAutoConfiguration
org.activiti.runtime.api.conf.ProcessRuntimeAutoConfiguration
org.activiti.core.common.spring.connector.autoconfigure.ConnectorAutoConfiguration
org.activiti.spring.process.conf.ProcessExtensionsConfiguratorAutoConfiguration
org.activiti.runtime.api.conf.CommonRuntimeAutoConfiguration
org.activiti.api.task.conf.impl.TaskModelAutoConfiguration
org.activiti.runtime.api.conf.TaskRuntimeAutoConfiguration
org.activiti.application.conf.ApplicationProcessAutoConfiguration
org.activiti.spring.boot.EndpointAutoConfiguration
org.activiti.spring.boot.ProcessEngineAutoConfiguration
org.activiti.spring.boot.ActivitiMethodSecurityAutoConfiguration
org.activiti.spring.process.conf.ProcessExtensionsAutoConfiguration
```

`结果：运行成功，且数据库也创建成功`

> 参考：https://blog.csdn.net/sinat_35670743/article/details/130964792?