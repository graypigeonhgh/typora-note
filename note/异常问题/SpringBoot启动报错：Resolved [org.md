# SpringBoot启动报错：Resolved [org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException

<br>

#### 具体报错代码如下：

```java
Resolved [org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException: Failed to convert value of type 'java.lang.String' to required type 'com.hgh.yuzhan.dto.EssaySearchDto'; Cannot convert value of type 'java.lang.String' to required type 'com.hgh.yuzhan.dto.EssaySearchDto': no matching editors or conversion strategy found]
```

<br>

#### 报错原因：@RequestBody只支持POST请求，GET请求不能使用@RequestBody；若使用GET请求，则需使用@RequestParam或@PathVariable