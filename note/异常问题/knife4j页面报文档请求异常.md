# knife4j页面报文档请求异常

<br>

![image-20231118211306172](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231118211306172.png)

![image-20231118212250334](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231118212250334.png)

#### 解决：删掉静态资源映射器

```java
@Configuration
@Slf4j
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("静态资源映射...");
       
    }
    
}
```

#### 原因：未知
