# 启动代码生成器时报错“Exception in thread "main" java.lang.NoClassDefFoundError”



#### 具体错误如下：

```java
Exception in thread "main" java.lang.NoClassDefFoundError: freemarker/template/Configuration
	at com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine.init(FreemarkerTemplateEngine.java:41)
	at com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine.init(FreemarkerTemplateEngine.java:36)
	at com.baomidou.mybatisplus.generator.AutoGenerator.execute(AutoGenerator.java:179)
	at com.baomidou.mybatisplus.generator.FastAutoGenerator.execute(FastAutoGenerator.java:229)
	at com.hgh.yuzhan.config.CodeGenerator.main(CodeGenerator.java:86)
Caused by: java.lang.ClassNotFoundException: freemarker.template.Configuration
	at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:641)
	at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:188)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:525)
	... 5 more
```



#### 原因：添加代码生成器时没有导入freemarker依赖

```java
<!-- 代码生成器 -->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.5.3.1</version>
</dependency>
<!-- 添加模版引擎依赖（Velocity默认) -->
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity-engine-core</artifactId>
    <version>2.3</version>
</dependency>
<!-- 代码生成器依赖包 -->
<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.31</version>
</dependency>
```



