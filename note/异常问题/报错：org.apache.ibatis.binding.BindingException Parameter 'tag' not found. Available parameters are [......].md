# 报错：org.apache.ibatis.binding.BindingException: Parameter 'tag' not found. Available parameters are [......]

<br>

#### 1.具体报错如下：

```java
org.apache.ibatis.binding.BindingException: Parameter 'tag' not found. Available parameters are [searchText, Tag, param1, param2]
	at org.apache.ibatis.binding.MapperMethod$ParamMap.get(MapperMethod.java:212) ~[mybatis-3.5.10.jar:3.5.10]
	at org.apache.ibatis.scripting.xmltags.DynamicContext$ContextAccessor.getProperty(DynamicContext.java:120) ~[mybatis-3.5.10.jar:3.5.10]
	at org.apache.ibatis.ognl.OgnlRuntime.getProperty(OgnlRuntime.java:3341) ~[mybatis-3.5.10.jar:3.5.10]
	at org.apache.ibatis.ognl.ASTProperty.getValueBody(ASTProperty.java:121) ~[mybatis-3.5.10.jar:3.5.10]
	at org.apache.ibatis.ognl.SimpleNode.evaluateGetValueBody(SimpleNode.java:212) ~[mybatis-
	......
```

<br>

#### 2.报错原因：由BindingException异常可知，为参数绑定异常，即传入的参数和xml文件中的sql语句中的参数不一致

![屏幕截图 2023-11-30 165224](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-11-30%20165224.jpg)

#### 3.解决办法

* ##### 添加@Param注解（总之注解中的参数和sql语句中的参数要一致）

![image-20231130165929096](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231130165929096.png)