# mybatis报错：Field 'id' doesn't have a default value

<br>

* ##### 本人在mybatis的xml文件中通过执行SQL语句进行新增操作（insert）时报错

<br>

#### 报错原因：大概是在xml文件中执行SQL语句时，主键字段没有配置自增生成策略，所以执行新增操作的时候，需要给id字段设置值，才能新增成功。新增成功后原先的主键id会被通过雪花算法生成的id覆盖

###### 具体操作如下：

```java
<insert id="saveTag">
    insert into tags (id,info_id, type, tag_name)
    value (123,#{infoId}, #{type}, #{tagName})
</insert>
```

