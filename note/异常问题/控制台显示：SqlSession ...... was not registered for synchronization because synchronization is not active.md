# 控制台显示：SqlSession ...... was not registered for synchronization because synchronization is not active  JDBC Connection ......will not be managed by Spring

<br>

#### 具体信息如下

```java
SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@118ae053] was not registered for synchronization because synchronization is not active
JDBC Connection [HikariProxyConnection@978265438 wrapping com.mysql.cj.jdbc.ConnectionImpl@39dd8c98] will not be managed by Spring
==>  Preparing: SELECT id,name,wechat_account,phone,password,email,sex,avatar,country,province,city,create_time,update_time,status FROM user WHERE id=?
==> Parameters: 1417414926166769669(Long)
<==    Columns: ...
<==        Row: ...
<==      Total: 1
Closing non transactional SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@118ae053]
```

<br>

#### 原因：未知

> 