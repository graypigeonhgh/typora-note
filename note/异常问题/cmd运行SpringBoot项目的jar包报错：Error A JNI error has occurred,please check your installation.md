# cmd运行SpringBoot项目的jar包报错：Error: A JNI error has occurred,please check your installation...

SpringBoot项目在idea运行正常，而打包成jar包后再cmd命令行运行报错，具体报错如下：

![fe63c97159194660b97ffca2be41d161](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/fe63c97159194660b97ffca2be41d161.png)

**原因:**较高版本的JDK编译的java class文件试图在较低版本的JVM上运行产生的错误。

> 可在cmd中执行：java -version  来查看本地配置的JDK版本
>
> 可在idea中的File—>Project Structure—>Project中查看jar包使用的JDK版本
>
> 对比两者版本是否一致

**不一致的解决办法：**配置电脑环境变量

![屏幕截图 2024-02-26 134808](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-02-26%20134808.png)

在环境变量中的path配置高版本的JDK的bin路径：

![image-20240226135231269](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240226135231269.png)

> 不要忘记加\bin：C:\Users\23718\.jdks\corretto-17.0.8.1\bin

最后在jar包所在的文件夹执行cmd命令：

![image-20240226135422657](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240226135422657.png)