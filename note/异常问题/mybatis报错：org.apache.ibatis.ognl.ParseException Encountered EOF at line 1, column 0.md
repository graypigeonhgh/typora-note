# mybatis报错：org.apache.ibatis.ognl.ParseException: Encountered "<EOF>" at line 1, column 0.

<br>

#### 具体报错代码如下：

```java
org.apache.ibatis.ognl.ParseException: Encountered "<EOF>" at line 1, column 0.
Was expecting one of:
    ":" ...
    "not" ...
    "+" ...
    "-" ...
    "~" ...
    "!" ...
    "(" ...
    "true" ...
    "false" ...
    "null" ...
    "#this" ...
    "#root" ...
    "#" ...
    "[" ...
    "{" ...
    "@" ...
    "new" ...
    <IDENT> ...
    <DYNAMIC_SUBSCRIPT> ...
    "\'" ...
    "`" ...
    "\"" ...
    <INT_LITERAL> ...
    <FLT_LITERAL> ...
    
```

<br>

#### 报错原因：在mybatis的xml文件的SQL语句中出现了问题，如使用了大写字符“OR”，“AND”等，或者语法错误（可以在数据库中测试成功后再放入xml文件中），如动态SQL的标签使用错误等。

```java
@RestController
@RequestMapping("/yuzhan/stars")
@Slf4j
public class StarsController {

    @Autowired
    private StarsService starsService;

    @PostMapping
    @Operation(summary = "新增收藏",description = "新增收藏")
    public R<String> save(@RequestBody Stars stars){
        log.info("stars:{}",stars);
        starsService.saveOrUpdate(stars);
        return R.success("新增收藏成功");
    }

    @DeleteMapping
    @Operation(summary = "删除收藏",description = "删除收藏")
    public R<String> delete(@RequestParam("id") Long id){
        log.info("删除收藏，id为：{}",id);
        starsService.removeById(id);
        return R.success("收藏删除成功");
    }

    @GetMapping("/page")
    @Operation(summary = "分页查询",description = "分页查询")
    public R<Page> page(int page, int pageSize){
        log.info("page = {}, pageSize = {}", page, pageSize);

        //构造分页构造器
        Page<Stars> pageInfo = new Page<>(page, pageSize);
        //构造条件构造器
        LambdaQueryWrapper<Stars> queryWrapper = new LambdaQueryWrapper<>();
        //添加排序条件（根据发布时间排序）
        queryWrapper.orderByDesc(Stars::getCreateTime);
        //执行分页查询
        starsService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

    @GetMapping("/list")
    @Operation(summary = "列表",description = "列表")
    public R<List<Stars>> list(Stars stars){
        return R.success(starsService.getStarList(stars));
    }

    @GetMapping("/{id}")
    @Operation(summary = "查询",description = "查询")
    public R<Stars> get(@PathVariable int id){
        log.info("根据id:{}查询收藏记录",id);

        Stars stars = starsService.getById(id);
        if (stars != null) {
            return R.success(stars);
        }
        return R.error("没有查询到对应收藏");
    }
}
```

```java
/**
     * 获取收藏列表
     * @param stars
     * @return java.util.List<com.hgh.yuzhan.entity.Stars>
     **/
    List<Stars> getStarList(Stars stars);

    /**
     * 新增/保存收藏
     * @param stars
     * @return java.lang.Boolean
     **/
    Boolean saveStar(Stars stars);

@Autowired
    public StarsMapper starsMapper;

    @Override
    public List<Stars> getStarList(Stars stars) {

        //构造条件构造器
        LambdaQueryWrapper<Stars> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤条件
        queryWrapper.eq(stars.getType() != null, Stars::getType, stars.getType());
        //添加排序条件
        queryWrapper.orderByDesc(Stars::getCreateTime);

        List<Stars> list = list(queryWrapper);
        return list;
    }

    @Override
    public Boolean saveStar(Stars stars) {
        return starsMapper.saveStar(stars.getUserId(), stars.getStarId(), stars.getType());
    }
```

