### SpringBoot项目运行报错：org.springframework.beans.factory.BeanNotOfRequiredTypeException......

![image-20240313115221386](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240313115221386.png)

版本配置：SpringBoot3.2.3 + MyBatisPlus3.5.4.1

**具体报错如下：**

> Error starting ApplicationContext. To display the condition evaluation report re-run your application with 'debug' enabled.
> 2024-03-13T11:46:38.294+08:00 ERROR 115216 --- [           main] o.s.boot.SpringApplication               : Application run failed
>
> org.springframework.beans.factory.BeanNotOfRequiredTypeException: Bean named 'ddlApplicationRunner' is expected to be of type 'org.springframework.boot.Runner' but was actually of type 'org.springframework.beans.factory.support.NullBean'
> 	at org.springframework.beans.factory.support.AbstractBeanFactory.adaptBeanInstance(AbstractBeanFactory.java:410) ~[spring-beans-6.1.4.jar:6.1.4]
> 	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:391) ~[spring-beans-6.1.4.jar:6.1.4]
> 	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:204) ~[spring-beans-6.1.4.jar:6.1.4]
> 	at org.springframework.boot.SpringApplication.callRunners(SpringApplication.java:770) ~[spring-boot-3.2.3.jar:3.2.3]
> 	at org.springframework.boot.SpringApplication.run(SpringApplication.java:341) ~[spring-boot-3.2.3.jar:3.2.3]
> 	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1354) ~[spring-boot-3.2.3.jar:3.2.3]
> 	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1343) ~[spring-boot-3.2.3.jar:3.2.3]
> 	at com.hgh.security_demo.SecurityDemoApplication.main(SecurityDemoApplication.java:10) ~[classes/:na]

**原因：**似乎SpringBoot版本过高，导致其内置的mybatis版本与MyBatisPlus不兼容导致

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.2.3</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.4.1</version>
</dependency>
```



**解决:**将SpringBoot版本降低为3.1.5

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>3.2.3</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.4.1</version>
</dependency>
```

