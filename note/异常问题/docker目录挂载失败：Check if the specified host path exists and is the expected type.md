# docker目录挂载失败：Check if the specified host path exists and is the expected type

docker目录挂载命令，其目的是为了达到修改linux上的文件同步到容器上，从而实现修改容器的配置文件。

在docker目录挂载或启动容器时报错，具体报错如下:

执行：

```shell
docker run -it --name mosquitto -p 1883:1883 -p 9001:9001 -v /mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf -v /mosquitto/data:/mosquitto/data -v /mosquitto/log:/mosquitto/log -d eclipse-mosquitto 
```



![image-20240330095328859](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240330095328859.png)

```shell
docker: Error response from daemon: failed to create task for container: failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: error during container init: error mounting "/mosquitto/config/mosquitto.conf" to rootfs at "/mosquitto/config/mosquitto.conf": mount /mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf (via /proc/self/fd/7), flags: 0x5000: not a directory: unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type.
```

他的意思是说我的/mosquitto/config/mosquitto.conf是目录，不能挂载

* 首先，你需要确认一下/mosquitto/config/mosquitto.conf这个路径是否存在，并且该路径下是否有mosquitto.conf
  这个文件。如果该文件不存在，则需要先创建该文件。
* 其次，你需要检查容器启动命令中关于挂载的部分是否正确。在这个错误提示中，可能是因为在挂载时将一个文件挂载到了一个目录上，或者是将一个目录挂载到了一个文件上，导致挂载失败。因此，你需要确认一下挂载的目标路径是否正确，以及该路径是否存在。

**报错原因：**

我是因为没有写全目录路径，不小心将mosquitto文件夹建在root路径下，应先使用pwd看清楚文件路径

修改后命令：

```shell
docker run -it --name mosquitto -p 1883:1883 -p 9001:9001 -v /root/mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf -v /root/mosquitto/data:/mosquitto/data -v /root/mosquitto/log:/mosquitto/log -d eclipse-mosquitto 
```

目录挂载成功

![image-20240330101903529](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240330101903529.png)