# java.sql.SQLException: Field 'user_id' doesn't have a default value

#### **具体报错如下：** 

![image-20240323223901655](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240323223901655.png)

#### 报错原因：

1、数据库表字段userId设置了不能为null，但写入数据时userId的值为null，导致报错。

解决办法：

（1）写数据不变，数据库表字段username设置为允许为null。

（2）数据库表字段不变，写数据是username的值不为null。

> 参考：https://blog.csdn.net/qq_38974638/article/details/120002074?