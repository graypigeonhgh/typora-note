# DockerCompose安装与使用

### 一、安装

#### 1.从github上下载docker-compose二进制文件安装

https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64

#### 2.把文件放入/usr/local/bin路径下

#### 3.执行命令

```powershell
mv docker-compose-linux-x86_64 docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose version
# 输出：Docker Compose version v2.5.0
```

>  参考：https://blog.csdn.net/qq_35995514/article/details/125468792

### 二、常用命令

```shell
docker-compose up
用于部署一个 Compose 应用

默认情况下该命令会读取名为 docker-compose.yml 或 docker-compose.yaml 的文件

当然用户也可以使用 -f 指定其他文件名。通常情况下，会使用 -d 参数令应用在后台启动

docker-compose stop
停止 Compose 应用相关的所有容器，但不会删除它们

被停止的应用可以很容易地通过 docker-compose restart 命令重新启动

docker-compose rm
用于删除已停止的 Compose 应用

它会删除容器和网络，但是不会删除卷和镜像

docker-compose restart
重启已停止的 Compose 应用

如果用户在停止该应用后对其进行了变更，那么变更的内容不会反映在重启后的应用中，这时需要重新部署应用使变更生效

docker-compose ps
用于列出 Compose 应用中的各个容器

输出内容包括当前状态、容器运行的命令以及网络端口

docker-compose down
停止并删除运行中的 Compose 应用

它会删除容器和网络，但是不会删除卷和镜像
```

三、文件配置

```yaml
1. image
services:
  web:
    image: hello-world
在 services 标签下的第二级标签是 web，这个名字是用户自己自定义，它就是服务名称。 image 则是指定服务的镜像名称或镜像 ID。如果镜像在本地不存在，Compose 将会尝试拉取这个镜像。 例如下面这些格式都是可以的：

image: redis
image: ubuntu:14.04
image: tutum/influxdb
image: example-registry.com:4000/postgresql
image: a4bc65fd
2. build
服务除了可以基于指定的镜像，还可以基于一份 Dockerfile，在使用 up 启动之时执行构建任务，这个构建标签就是 build，它可以指定 Dockerfile 所在文件夹的路径。Compose 将会利用它自动构建这个镜像，然后使用这个镜像启动服务容器。

build: /path/to/build/dir
也可以是相对路径，只要上下文确定就可以读取到 Dockerfile。

build: ./dir
设定上下文根目录，然后以该目录为准指定 Dockerfile。

build:
  context: ../
  dockerfile: path/of/Dockerfile
注意 build 都是一个目录，如果你要指定 Dockerfile 文件需要在 build 标签的子级标签中使用 dockerfile 标签指定，如上面的例子。 如果你同时指定了 image 和 build 两个标签，那么 Compose 会构建镜像并且把镜像命名为 image 后面的那个名字。

build: ./dir
image: webapp:tag
既然可以在 docker-compose.yml 中定义构建任务，那么一定少不了 arg 这个标签，就像 Dockerfile 中的 ARG 指令，它可以在构建过程中指定环境变量，但是在构建成功后取消，在 docker-compose.yml 文件中也支持这样的写法：

build:
  context: .
  args:
    buildno: 1
    password: secret
下面这种写法也是支持的，一般来说下面的写法更适合阅读。

build:
  context: .
  args:
    - buildno=1
    - password=secret
与 ENV 不同的是，ARG 是允许空值的。例如：

args:
  - buildno
  - password
这样构建过程可以向它们赋值。

注意：YAML 的布尔值（true, false, yes, no, on, off）必须要使用引号引起来（单引号、双引号均可），否则会当成字符串解析。

3. command
使用 command 可以覆盖容器启动后默认执行的命令。

command: bundle exec thin -p 3000
也可以写成类似 Dockerfile 中的格式：

command: [bundle, exec, thin, -p, 3000]
4.container_name
前面说过 Compose 的容器名称格式是：<项目名称><服务名称><序号> 虽然可以自定义项目名称、服务名称，但是如果你想完全控制容器的命名，可以使用这个标签指定：

container_name: app
这样容器的名字就指定为 app 了。

5.depends_on
在使用 Compose 时，最大的好处就是少打启动命令，但是一般项目容器启动的顺序是有要求的，如果直接从上到下启动容器，必然会因为容器依赖问题而启动失败。 例如在没启动数据库容器的时候启动了应用容器，这时候应用容器会因为找不到数据库而退出，为了避免这种情况我们需要加入一个标签，就是 depends_on，这个标签解决了容器的依赖、启动先后的问题。 例如下面容器会先启动 redis 和 db 两个服务，最后才启动 web 服务：

version: '2'
services:
  web:
    build: .
    depends_on:
      - db
      - redis
  redis:
    image: redis
  db:
    image: postgres
注意的是，默认情况下使用 docker-compose up web 这样的方式启动 web 服务时，也会启动 redis 和 db 两个服务，因为在配置文件中定义了依赖关系。

6.dns
和--dns参数一样用途，格式如下：

dns: 8.8.8.8
也可以是一个列表：

dns:
  - 8.8.8.8
  - 9.9.9.9
此外 dns_search 的配置也类似：

dns_search: example.com
dns_search:
  - dc1.example.com
  - dc2.example.com
7. tmpfs
挂载临时目录到容器内部，与 run 的参数一样效果：

tmpfs: /run
tmpfs:
  - /run
  - /tmp
8. expose
这个标签与Dockerfile中的EXPOSE指令一样，用于指定暴露的端口，但是只是作为一种参考，实际上docker-compose.yml的端口映射还得ports这样的标签。

expose:
 - "3000"
 - "8000"
9. external_links
在使用Docker过程中，我们会有许多单独使用docker run启动的容器，为了使Compose能够连接这些不在docker-compose.yml中定义的容器，我们需要一个特殊的标签，就是external_links，它可以让Compose项目里面的容器连接到那些项目配置外部的容器（前提是外部容器中必须至少有一个容器是连接到与项目内的服务的同一个网络里面）。 格式如下：

external_links:
 - redis_1
 - project_db_1:mysql
 - project_db_1:postgresql
10. extra_hosts
添加主机名的标签，就是往/etc/hosts文件中添加一些记录，与Docker client的--add-host类似：

extra_hosts:
 - "somehost:162.242.195.82"
 - "otherhost:50.31.209.229"
启动之后查看容器内部hosts：

162.242.195.82  somehost
50.31.209.229   otherhost
复制Error复制成功...
11. logging
这个标签用于配置日志服务。格式如下：

logging:
  driver: syslog
  options:
    syslog-address: "tcp://192.168.0.42:123"
默认的driver是json-file。只有json-file和journald可以通过docker-compose logs显示日志，其他方式有其他日志查看方式，但目前Compose不支持。对于可选值可以使用options指定。 有关更多这方面的信息可以阅读官方文档： https://docs.docker.com/engine/admin/logging/overview/

12. ports
映射端口的标签。 使用HOST:CONTAINER格式或者只是指定容器的端口，宿主机会随机映射端口。

ports:
 - "3000"
 - "8000:8000"
 - "49100:22"
 - "127.0.0.1:8001:8001"
13. volumes
挂载一个目录或者一个已存在的数据卷容器，可以直接使用 [HOST:CONTAINER] 这样的格式，或者使用 [HOSTCONTAINERro] 这样的格式，后者对于容器来说，数据卷是只读的，这样可以有效保护宿主机的文件系统。 Compose的数据卷指定路径可以是相对路径，使用.或者..来指定相对目录。 数据卷的格式可以是下面多种形式：

volumes:
  // 只是指定一个路径，Docker 会自动在创建一个数据卷（这个路径是容器内部的）。
  - /var/lib/mysql

  // 使用绝对路径挂载数据卷
  - /opt/data:/var/lib/mysql

  // 以 Compose 配置文件为中心的相对路径作为数据卷挂载到容器。
  - ./cache:/tmp/cache

  // 使用用户的相对路径（~/ 表示的目录是 /home/<用户目录>/ 或者 /root/）。
  - ~/configs:/etc/configs/:ro

  // 已经存在的命名的数据卷。
  - datavolume:/var/lib/mysql
如果你不使用宿主机的路径，你可以指定一个volume_driver。

volume_driver: mydriver
14. volumes_from
从其它容器或者服务挂载数据卷，可选的参数是 :ro或者 :rw，前者表示容器只读，后者表示容器对数据卷是可读可写的。默认情况下是可读可写的。

volumes_from:
  - service_name
  - service_name:ro
  - container:container_name
  - container:container_name:rw
15. network_mode
网络模式，与Docker client的--net参数类似，只是相对多了一个service:[service name] 的格式。 例如：

network_mode: "bridge"
network_mode: "host"
network_mode: "none"
network_mode: "service:[service name]"
network_mode: "container:[container name/id]"
可以指定使用服务或者容器的网络

16. networks
加入指定网络，格式如下：

services:
  some-service:
    networks:
     - some-network
     - other-network
关于这个标签还有一个特别的子标签aliases，这是一个用来设置服务别名的标签，例如：

services:
  some-service:
    networks:
      some-network:
        aliases:
         - alias1
         - alias3
      other-network:
        aliases:
         - alias2
相同的服务可以在不同的网络有不同的别名

```

> 参考：https://blog.csdn.net/qq_45868731/article/details/131743699?