# markdown常用html代码

## 一、用户表格呈现

#### 代码：

```html
<table>
	<tbody>
		<tr>
            <td align="center">
                <a href="https://github.com/nihaojob">
                    <img src="https://avatars.githubusercontent.com/u/13534626?v=4" width="80;" alt="nihaojob"/>
                    <br />
                    <sub><b>nihaojob</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/Qiu-Jun">
                    <img src="https://avatars.githubusercontent.com/u/24954362?v=4" width="80;" alt="Qiu-Jun"/>
                    <br />
                    <sub><b>Qiu-Jun</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/wuchenguang1998">
                    <img src="https://avatars.githubusercontent.com/u/63847336?v=4" width="80;" alt="wuchenguang1998"/>
                    <br />
                    <sub><b>wuchenguang1998</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/AliceLanniste">
                    <img src="https://avatars.githubusercontent.com/u/17617116?v=4" width="80;" alt="AliceLanniste"/>
                    <br />
                    <sub><b>AliceLanniste</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ylx252">
                    <img src="https://avatars.githubusercontent.com/u/6425957?v=4" width="80;" alt="ylx252"/>
                    <br />
                    <sub><b>ylx252</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/liumingye">
                    <img src="https://avatars.githubusercontent.com/u/8676207?v=4" width="80;" alt="liumingye"/>
                    <br />
                    <sub><b>liumingye</b></sub>
                </a>
            </td>
		</tr>
		<tr>
            <td align="center">
                <a href="https://github.com/momo2019">
                    <img src="https://avatars.githubusercontent.com/u/26078793?v=4" width="80;" alt="momo2019"/>
                    <br />
                    <sub><b>momo2019</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ByeWord">
                    <img src="https://avatars.githubusercontent.com/u/37115721?v=4" width="80;" alt="ByeWord"/>
                    <br />
                    <sub><b>ByeWord</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/bigFace2019">
                    <img src="https://avatars.githubusercontent.com/u/55651401?v=4" width="80;" alt="bigFace2019"/>
                    <br />
                    <sub><b>bigFace2019</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/wohuweixiya">
                    <img src="https://avatars.githubusercontent.com/u/86701050?v=4" width="80;" alt="wohuweixiya"/>
                    <br />
                    <sub><b>wohuweixiya</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/zjc2233">
                    <img src="https://avatars.githubusercontent.com/u/43945226?v=4" width="80;" alt="zjc2233"/>
                    <br />
                    <sub><b>zjc2233</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ijry">
                    <img src="https://avatars.githubusercontent.com/u/3102798?v=4" width="80;" alt="ijry"/>
                    <br />
                    <sub><b>ijry</b></sub>
                </a>
            </td>
		</tr>
	<tbody>
</table>
```

#### 效果展示(以下仅供参考，侵权删)

<table>
	<tbody>
		<tr>
            <td align="center">
                <a href="https://github.com/nihaojob">
                    <img src="https://avatars.githubusercontent.com/u/13534626?v=4" width="80;" alt="nihaojob"/>
                    <br />
                    <sub><b>nihaojob</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/Qiu-Jun">
                    <img src="https://avatars.githubusercontent.com/u/24954362?v=4" width="80;" alt="Qiu-Jun"/>
                    <br />
                    <sub><b>Qiu-Jun</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/wuchenguang1998">
                    <img src="https://avatars.githubusercontent.com/u/63847336?v=4" width="80;" alt="wuchenguang1998"/>
                    <br />
                    <sub><b>wuchenguang1998</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/AliceLanniste">
                    <img src="https://avatars.githubusercontent.com/u/17617116?v=4" width="80;" alt="AliceLanniste"/>
                    <br />
                    <sub><b>AliceLanniste</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ylx252">
                    <img src="https://avatars.githubusercontent.com/u/6425957?v=4" width="80;" alt="ylx252"/>
                    <br />
                    <sub><b>ylx252</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/liumingye">
                    <img src="https://avatars.githubusercontent.com/u/8676207?v=4" width="80;" alt="liumingye"/>
                    <br />
                    <sub><b>liumingye</b></sub>
                </a>
            </td>
		</tr>
		<tr>
            <td align="center">
                <a href="https://github.com/momo2019">
                    <img src="https://avatars.githubusercontent.com/u/26078793?v=4" width="80;" alt="momo2019"/>
                    <br />
                    <sub><b>momo2019</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ByeWord">
                    <img src="https://avatars.githubusercontent.com/u/37115721?v=4" width="80;" alt="ByeWord"/>
                    <br />
                    <sub><b>ByeWord</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/bigFace2019">
                    <img src="https://avatars.githubusercontent.com/u/55651401?v=4" width="80;" alt="bigFace2019"/>
                    <br />
                    <sub><b>bigFace2019</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/wohuweixiya">
                    <img src="https://avatars.githubusercontent.com/u/86701050?v=4" width="80;" alt="wohuweixiya"/>
                    <br />
                    <sub><b>wohuweixiya</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/zjc2233">
                    <img src="https://avatars.githubusercontent.com/u/43945226?v=4" width="80;" alt="zjc2233"/>
                    <br />
                    <sub><b>zjc2233</b></sub>
                </a>
            </td>
            <td align="center">
                <a href="https://github.com/ijry">
                    <img src="https://avatars.githubusercontent.com/u/3102798?v=4" width="80;" alt="ijry"/>
                    <br />
                    <sub><b>ijry</b></sub>
                </a>
            </td>
		</tr>
	<tbody>
</table>



### 二、简单介绍

#### 代码：

```html
<p align="center">
  <a href="https://pro.kuaitu.cc" target="_blank">
    <img src="https://github.com/user-attachments/assets/4e519179-8d19-41cc-ad2b-a1d7ebc63836" width="318px" alt="开源图片编辑器" />
  </a>
</p>
<h3 align="center">开源图片编辑器 · 插件化架构 · 拖拽式设计 · 功能完善 </h3>
<p align="center">基于 fabric.js 和 Vue 开发的插件化图片编辑器，可自定义字体、素材、设计模板、右键菜单、快捷键</p>
<p align="center"><a href="https://ikuaitu.github.io/vue-fabric-editor/" target="_blank">演示</a> · <a href="https://ikuaitu.github.io/doc/#/"  target="_blank">文档</a> · <a href="https://www.kuaitu.cc/"  target="_blank">付费版演示</a> · <a href="https://pro.kuaitu.cc/"  target="_blank">付费版介绍</a>
</p>
```



#### 效果展示（仅供参考）：

<p align="center">
  <a href="https://pro.kuaitu.cc" target="_blank">
    <img src="https://github.com/user-attachments/assets/4e519179-8d19-41cc-ad2b-a1d7ebc63836" width="318px" alt="开源图片编辑器" />
  </a>
</p>
<h3 align="center">开源图片编辑器 · 插件化架构 · 拖拽式设计 · 功能完善 </h3>
<p align="center">基于 fabric.js 和 Vue 开发的插件化图片编辑器，可自定义字体、素材、设计模板、右键菜单、快捷键</p>
<p align="center"><a href="https://ikuaitu.github.io/vue-fabric-editor/" target="_blank">演示</a> · <a href="https://ikuaitu.github.io/doc/#/"  target="_blank">文档</a> · <a href="https://www.kuaitu.cc/"  target="_blank">付费版演示</a> · <a href="https://pro.kuaitu.cc/"  target="_blank">付费版介绍</a>
</p>



### 三、彩色徽章

#### 代码：

```html
<p align="center">
  <a href="" target="_blank">
    <img src="https://img.shields.io/github/stars/ikuaitu/vue-fabric-editor?style=flat" alt="stars" />
  </a>
  <a href="" target="_blank">
    <img src="https://img.shields.io/github/forks/ikuaitu/vue-fabric-editor?style=flat" alt="stars" />
  </a>
  <a href="https://github.com/ikuaitu/vue-fabric-editor/graphs/contributors" target="_blank">
    <img src="https://img.shields.io/github/contributors/ikuaitu/vue-fabric-editor" alt="contributors" />
  </a>
  <a href="https://github.com/ikuaitu/vue-fabric-editor?tab=MIT-1-ov-file" target="_blank">
    <img src="https://img.shields.io/github/license/ikuaitu/vue-fabric-editor?style=flat" alt="license" />
  </a>
  <a href="https://www.kuaitu.cc/" target="_blank">
    <img src="https://img.shields.io/website?url=http%3A%2F%2Fpro.kuaitu.cc%2F" alt="快图设计网站" />
  </a>
</p>
```

#### 效果展示：

<p align="center">
  <a href="" target="_blank">
    <img src="https://img.shields.io/github/stars/ikuaitu/vue-fabric-editor?style=flat" alt="stars" />
  </a>
  <a href="" target="_blank">
    <img src="https://img.shields.io/github/forks/ikuaitu/vue-fabric-editor?style=flat" alt="stars" />
  </a>
  <a href="https://github.com/ikuaitu/vue-fabric-editor/graphs/contributors" target="_blank">
    <img src="https://img.shields.io/github/contributors/ikuaitu/vue-fabric-editor" alt="contributors" />
  </a>
  <a href="https://github.com/ikuaitu/vue-fabric-editor?tab=MIT-1-ov-file" target="_blank">
    <img src="https://img.shields.io/github/license/ikuaitu/vue-fabric-editor?style=flat" alt="license" />
  </a>
  <a href="https://www.kuaitu.cc/" target="_blank">
    <img src="https://img.shields.io/website?url=http%3A%2F%2Fpro.kuaitu.cc%2F" alt="快图设计网站" />
  </a>
</p>

> **彩色徽章网站**：https://shields.io/



> 参考：https://github.com/ikuaitu/vue-fabric-editor/blob/main/README.md