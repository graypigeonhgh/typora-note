# 基于knife4j的Swagger2配置说明

>Swagger是一款RESTFUL接口的文档在线自动生成+功能测试功能软件。Swagger是一个规范和完整的框架,用于生成、描述、调用和可视化RESTful风格的Web服务。目标是使客户端和文件系统作为服务器以同样的速度来更新文件的方法,参数和模型紧密集成到服务器。
>
>这个解释简单点来讲就是说，swagger是一款可以根据resutful风格生成的生成的接口开发文档，并且支持做测试的一款中间软件。

springboot3开始javax包改成了jakarta，而swagger-oas等包中依然使用的是javax所以报错。另外springfox已经过时了，两年没更新了，并且不支持OpenAPI3 标准，所以要迁移到springdoc

#### 配置：SpringBoot：2.7.16 + knife4j：3.0.3



#### 1.导入依赖（SpringBoot2）

```java

<!--knife4j是为java MVC框架集成Swagger生成Api文档的增强解决方案-->
<!-- https://mvnrepository.com/artifact/com.github.xiaoymin/knife4j-spring-boot-starter -->
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-boot-starter</artifactId>
    <version>3.0.3</version>
</dependency>
```



#### 2.在application.yml中设置相关配置：

```java
#集成Swagger高版本启动异常配置，低版本可以不配置
spring:
  mvc:
    pathmatch:
      matching-strategy: ANT_PATH_MATCHER
```



#### 3.Swagger注解说明

>@Api(tags="")
>用在请求的类上，表示对类的说明
>tags"说明该类的作用，可以在UI界面上看到的注解"
>
>@ApiOperation(value="")
>用在请求的方法上，说明方法的用途、作用
>value=“说明方法的用途、作用”
>
>@ApiImplicitParams
>用在请求的方法上，表示一组参数说明
>
>@ApiImplicitParam
>@ApiImplicitParam:指定一个请求参数的各个方面
>value：参数的汉字说明、解释
>required：参数是否必须传
>paramType：参数放在哪个地方
>header –> 请求头的获取：@·RequestHeader
>query –> 请求参数的获取：@RequestParam
>path（用于restful接口）–> 请求路径变量的获取：
>
>@PathVariable
>body（不常用）
>form（不常用）
>dataType：参数类型，默认String，其它值dataType=“Integer”
>defaultValue：参数的默认值
>
>@ApiResponses
>用在请求的方法上，表示一组响应
>
>@ApiResponse
>用在@ApiResponses中，一般用于表达一个错误的响应信息
>code：数字，例如400
>message：信息，例如"请求参数没填好"
>response：抛出异常的类
>
>@ApiModel
>主要有两种用途：
>用于响应类上，表示一个返回响应数据的信息
>入参实体：使用@RequestBody这样的场景， 请求参数无法使用@ApiImplicitParam注解进行描述的时候
>
>@ApiModelProperty
>用在属性上，描述响应类的属性

###### 参考：https://blog.csdn.net/yy12345_6_/article/details/125470205



#### 4.在config包下添加配置文件（SwaggerConfig)

```java
@EnableSwagger2
@Configuration
@EnableKnife4j
public class SwaggerConfig {
	// 配置docket以配置Swagger具体参数
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.tool.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    // 构建 api文档的详细信息函数
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("swagger文档标题")
                .description("swagger文档注释")
                .termsOfServiceUrl("服务地址")
                .contact(new Contact("GrayPigeonHGH", "url地址", "2371849349@qq.com"))
                .version("1.0")
                .build();
    }
}
 
```



#### 5.访问路径：http://localhost:8080/swagger-ui.html

##### 高版本的访问路径改变：http://localhost:8080/swagger-ui/index.html



#### 6.Swagger配置Docket的常用参数列表

```java
public class Docket implements DocumentationPlugin {
    public static final String DEFAULT_GROUP_NAME = "default";
    private final DocumentationType documentationType;
    private final List<SecurityContext> securityContexts = new ArrayList();
    private final Map<RequestMethod, List<ResponseMessage>> responseMessages = new HashMap();
    private final Map<HttpMethod, List<Response>> responses = new HashMap();
    private final List<Parameter> globalOperationParameters = new ArrayList();	//全局配置参数
    private final List<Function<TypeResolver, AlternateTypeRule>> ruleBuilders = new ArrayList();
    private final Set<Class> ignorableParameterTypes = new HashSet();	// 配置接口忽略参数
    private final Set<String> protocols = new HashSet();
    private final Set<String> produces = new LinkedHashSet();
    private final Set<String> consumes = new LinkedHashSet();
    private final Set<ResolvedType> additionalModels = new HashSet();
    private final Set<Tag> tags = new HashSet();
    private final List<Server> servers = new ArrayList();
    private PathProvider pathProvider;
    private List<SecurityScheme> securitySchemes;
    private Comparator<ApiListingReference> apiListingReferenceOrdering;
    private Comparator<ApiDescription> apiDescriptionOrdering;
    private Comparator<Operation> operationOrdering;
    private ApiInfo apiInfo;
    private String groupName;	//设置分组
    private boolean enabled;	//设置要显示的Swagger环境
    private GenericTypeNamingStrategy genericsNamingStrategy;
    private boolean applyDefaultResponseMessages;
    private String host;
    private Optional<String> pathMapping;
    private ApiSelector apiSelector;
    private boolean enableUrlTemplating;
    private final List<VendorExtension> vendorExtensions;
    private final List<RequestParameter> globalRequestParameters;	//全局请求参数
    
     public Docket(DocumentationType documentationType) {
        this.apiInfo = ApiInfo.DEFAULT;
        this.groupName = "default";
        this.enabled = true;
        this.genericsNamingStrategy = new DefaultGenericTypeNamingStrategy();
        this.applyDefaultResponseMessages = true;
        this.host = "";
        this.pathMapping = Optional.empty();
        this.apiSelector = ApiSelector.DEFAULT;
        this.enableUrlTemplating = false;
        this.vendorExtensions = new ArrayList();
        this.globalRequestParameters = new ArrayList();
        this.documentationType = documentationType;
    }
    ......
}

```

