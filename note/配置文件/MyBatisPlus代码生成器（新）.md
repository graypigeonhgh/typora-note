# MyBatisPlus代码生成器（新）

> 注意:
>
> 适用版本：mybatis-plus-generator 3.5.1 以上版本
>
> 参考：[官网](https://baomidou.com/pages/779a6e/)

本次配置：JDK17 + SpringBoot3.1.5 + MyBatisPlus3.5.3.1

**注意：**==mybatis-plus-generator版本需与mybatis-plus版本一致==

> 最新依赖参考：https://mvnrepository.com/artifact/com.baomidou/mybatis-plus-generator

#### 导入依赖

```java
<!-- 代码生成器 -->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.5.3.1</version>
</dependency>
<!-- 添加模版引擎依赖（Velocity默认) -->
<dependency>
    <groupId>org.apache.velocity</groupId>
    <artifactId>velocity-engine-core</artifactId>
    <version>2.3</version>
</dependency>
<!-- 代码生成器依赖包 -->
<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.31</version>
</dependency>
```



#### 官网代码生成器

```java
public class CodeGenerator {

    public static void main(String[] args) {
        
        // 代码生成器
        FastAutoGenerator.create("url", "username", "password")
                .globalConfig(builder -> {      //全局配置
                    builder.author("baomidou") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D://"); // 指定输出目录
                })
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    if (typeCode == Types.SMALLINT) {
                        // 自定义类型转换
                        return DbColumnType.INTEGER;
                    }
                    return typeRegistry.getColumnType(metaInfo);

                }))
                .packageConfig(builder -> {     //包配置
                    builder.parent("com.baomidou.mybatisplus.samples.generator") // 设置父包名
                            .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "D://")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {    //策略配置
                    builder.addInclude("t_simple") // 设置需要生成的表名，可以是list
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                            
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}

```

* create方法需要传入，数据库地址（如果你的MySQL版本为8，必需要在数据库地址后面加上时区, 像serverTimezone=Asia/Shanghai)

  

#### 包配置

![image-20211006153717464](https://img-blog.csdnimg.cn/img_convert/142d63a44b5e5b61464aaf7414d41f25.png)

#### 策略配置

![image-20211006155301030](https://img-blog.csdnimg.cn/img_convert/421d182015b73dce9227ee817e164a02.png)

#### 代码生成器配置完整代码

```java
public class CodeGenerator {

    public static void main(String[] args) {
        //设置tables集合，配置多个表
        List<String> tables = new ArrayList<>();
        tables.add("user");
        tables.add("trends");
        tables.add("trend_stars");
        tables.add("trend_likes");
        tables.add("trend_comments");

        // 代码生成器
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/yuzhan?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&useSSL=false&allowPublicKeyRetrieval=true",
                        "root", "a1234567")
                .globalConfig(builder -> {      //全局配置
                    builder.author("GrayPigeonHGH")     // 设置作者
                            .enableSpringdoc()    // 开启 Springdoc注解模式
                            .commentDate("yyyy年MM月dd日")     //格式化时间格式
                            .outputDir("D:\\java_code\\Git-Repository\\yuzhan\\yuzhan_android\\src\\main\\java"); // 指定模块Java目录的绝对路径
                })
                .dataSourceConfig(builder -> builder.typeConvertHandler((globalConfig, typeRegistry, metaInfo) -> {
                    int typeCode = metaInfo.getJdbcType().TYPE_CODE;
                    if (typeCode == Types.SMALLINT) {
                        // 自定义类型转换
                        return DbColumnType.INTEGER;
                    }
                    return typeRegistry.getColumnType(metaInfo);

                }))
                .packageConfig(builder -> {     //包配置
                    builder.parent("com.hgh") // 设置父包名
                            .moduleName("yuzhan") // 设置父包模块名
                            .entity("entity")   //以下包名都为默认值
                            .service("service")
                            .serviceImpl("service.impl")
                            .mapper("mapper")
                            .xml("mapper.xml")
                            .controller("controller")
                            .pathInfo(Collections.singletonMap(OutputFile.xml,"D:\\java_code\\Git-Repository\\yuzhan\\yuzhan_android\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {    //策略配置
                    builder.addInclude(tables) // 设置需要生成的表名，可以是list（table没有双引号）
                            .addTablePrefix("t_") // 设置过滤表前缀
                            .controllerBuilder()  //controller配置
                            .formatFileName("%sController")   //格式化 controller 文件名称
                            .enableFileOverride()   //覆盖已生成文件
                            .enableHyphenStyle()   //开启驼峰转连字符
                            .enableRestStyle()  //开启生成@RestController控制器
                            .mapperBuilder()   //mapper配置
                            .enableMapperAnnotation()   //开启 @Mapper 注解
                            .enableFileOverride()   //覆盖已生成文件
                            .formatMapperFileName("%sMapper")   //格式化 mapper 文件名称
                            .formatXmlFileName("%sMapper")    //格式化 xml 文件名称
                            .enableBaseResultMap()  //启用 BaseResultMap 生成
                            .superClass(BaseMapper.class)   //设置父类
                            .serviceBuilder()   //service配置
                            .enableFileOverride()   //覆盖已生成文件
                            .formatServiceFileName("%sService")   //格式化 service 文件名称
                            .formatServiceImplFileName("%sServiceImpl")   //格式化 serviceImpl 文件名称
                            .superServiceClass(IService.class)  //设置 service 接口父类
                            .entityBuilder()    //entity配置
                            .enableFileOverride()   //覆盖已生成文件
                            .enableLombok()     //开启lombok
                            .logicDeleteColumnName("is_deleted") //逻辑删除字段名
                            .enableTableFieldAnnotation();     //开启生成实体时生成字段注解

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}

```

> 参考：[Mybatis-plus最新代码生成器的使用](https://blog.csdn.net/qq_42682745/article/details/120626012?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522169958305816800184144368%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=169958305816800184144368&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-120626012-null-null.142^v96^pc_search_result_base9&utm_term=mybatisplus%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A8%E6%96%B0&spm=1018.2226.3001.4187)
>
> [官网](https://baomidou.com/pages/981406/#%E5%8F%AF%E9%80%89%E9%85%8D%E7%BD%AE)

