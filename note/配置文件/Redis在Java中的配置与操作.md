# Redis在Java中的操作



#### 1.配置Jedis环境

```java
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.8.0</version>
</dependency>
```



#### 2.springboot项目配置redis环境

```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <version>3.1.2</version>
    <scope>compile</scope>
</dependency>
```



#### 3.普通maven项目使用Jedis

```java
public class JedisTest {

    @Test
    public void testRedis(){
        //1.获取连接
        Jedis jedis = new Jedis("localhost");

        //2. 执行具体的操作
        jedis.set("username","xiaoming");
        String value = jedis.get("username");
        System.out.println(value);

        jedis.hset("myhash","test1","bj");
        String hValue = jedis.hget("myhash", "test1");
        System.out.println(hValue);

        Set<String> keys = jedis.keys("*");
        for(String key : keys){
            System.out.println(key);
        }
        //3. 关闭连接
        jedis.close();
    }
}
```



#### 4.SpringBoot项目使用Jedis

##### 4.1 在application.yml中设置以下配置

```java
spring:
  application:
	#应用名称：可选
    name: springdataredis_demo
#Redis相关配置
redis:
host: localhost
port: 6379
#password: a1234567
database: 0   #操作的是0号数据库
jedis:
  #Redis.连接池配置
  pool:
    max-active: 8  #最大连接数
    maX-wait: 1ms  #连接池最大阻塞等待时间
    maX-id几e: 4  #连接池中的最大空闲连接
    min-id几e: 0  #连接池中的最小空闲连接
```



##### 4.2 Redis配置类(RedisConfig.java)：配置Redis的Key序列化器，values可不设，因为在java程序中输出时会自动反序列化

```java
/**
 * Redis配置类
 */

@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {

        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();

        //默认的Key序列化器为：JdkSerializationRedisSerializer
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());

        redisTemplate.setConnectionFactory(connectionFactory);

        return redisTemplate;
    }

}
```



##### 4.3 SpringBoot中使用Redis

```java
@SpringBootTest
@RunWith(SpringRunner.class)
public class SpringDataRedisTest {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * @description: 操作String类型数据
     * @param:   []
     * @return:  void
     * @date:    2023/11/2 22:26
     **/
    @Test
    public void testString(){
        redisTemplate.opsForValue().set("city","beijing");

        String value = (String) redisTemplate.opsForValue().get("city");
        System.out.println(value);

        //SETEX key seconds value :设置指定key的值，并将key的过期时间设为seconds秒
        redisTemplate.opsForValue().set("key1","value1",20l, TimeUnit.SECONDS);

        //SETNX key value :只有在key不存在时设置key的值
        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent("name1", "xiaoming");
        System.out.println(aBoolean);
    }

    /**
     * 操作Hash类型数据
     */
    @Test
    public void testHash(){
        HashOperations hashOperations = redisTemplate.opsForHash();

        //存值
        hashOperations.put("002","name","xiaoming");
        hashOperations.put("002","age","20");
        hashOperations.put("002","address","bj");

        //取值
        String age = (String) hashOperations.get("002", "age");
        System.out.println(age);

        //获得hash结构中的所有字段
        Set keys = hashOperations.keys("002");
        for (Object key : keys) {
            System.out.println(key);
        }

        //获得hash结构中的所有值
        List values = hashOperations.values("002");
        for (Object value : values) {
            System.out.println(value);
        }
    }

    /**
     * 操作List类型的数据
     */
    @Test
    public void testList(){
        ListOperations listOperations = redisTemplate.opsForList();

        //存值
        listOperations.leftPush("mylist","a");
        listOperations.leftPushAll("mylist","b","c","d");

        //取值
        List<String> mylist = listOperations.range("mylist", 0, -1);
        for (String value : mylist) {
            System.out.println(value);
        }

        //获得列表长度 llen
        Long size = listOperations.size("mylist");
        int lSize = size.intValue();
        for (int i = 0; i < lSize; i++) {
            //出队列
            String element = (String) listOperations.rightPop("mylist");
            System.out.println(element);
        }
    }

    /**
     * 操作Set类型的数据
     */
    @Test
    public void testSet(){
        SetOperations setOperations = redisTemplate.opsForSet();

        //存值
        setOperations.add("myset","a","b","c","a");

        //取值
        Set<String> myset = setOperations.members("myset");
        for (String o : myset) {
            System.out.println(o);
        }

        //删除成员
        setOperations.remove("myset","a","b");

        //取值
        myset = setOperations.members("myset");
        for (String o : myset) {
            System.out.println(o);
        }

    }

    /**
     * 操作ZSet类型的数据
     */
    @Test
    public void testZset(){
        ZSetOperations zSetOperations = redisTemplate.opsForZSet();

        //存值
        zSetOperations.add("myZset","a",10.0);
        zSetOperations.add("myZset","b",11.0);
        zSetOperations.add("myZset","c",12.0);
        zSetOperations.add("myZset","a",13.0);

        //取值
        Set<String> myZset = zSetOperations.range("myZset", 0, -1);
        for (String s : myZset) {
            System.out.println(s);
        }

        //修改分数
        zSetOperations.incrementScore("myZset","b",20.0);

        //取值
        myZset = zSetOperations.range("myZset", 0, -1);
        for (String s : myZset) {
            System.out.println(s);
        }

        //删除成员
        zSetOperations.remove("myZset","a","b");

        //取值
        myZset = zSetOperations.range("myZset", 0, -1);
        for (String s : myZset) {
            System.out.println(s);
        }
    }

    /**
     * 通用操作，针对不同的数据类型都可以操作
     */
    @Test
    public void testCommon(){
        //获取Redis中所有的key
        Set<String> keys = redisTemplate.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }

        //判断某个key是否存在
        Boolean itcast = redisTemplate.hasKey("itcast");
        System.out.println(itcast);

        //删除指定key
        redisTemplate.delete("myZset");

        //获取指定key对应的value的数据类型
        DataType dataType = redisTemplate.type("myset");
        System.out.println(dataType.name());

    }
}
```

