# git常用命令

#### 一、获取日志并回退到历史版本

##### 1.获取历史日志

```shell
git reflog
```

##### 2.回退到历史版本

```shell
git reset --hard HEAD@{n}
```

* HEAD@{n}    其中一个历史版本号（n越大，历史版本越早）
  * 如`HEAD@{2}`表示`HEAD`指针在两次移动之前的情况；

##### 3.删除初始化文件（慎用）

```shell
rm -rf .git
```

#### 二、git上传代码到仓库

##### 1.新建仓库，并复制仓库地址

![image-20240329133106568](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240329133106568.png)

##### 2.主要命令（第一次提交）

1. git init      （初始化仓库）
2. git add .     (这里的.表示添加所有文件，也可以自定义添加，**点前面是有空格的**）
3. git commit -m ‘添加的注释信息’     （提交到本地仓库并添加注释信息）
4. git remote add origin ‘url’   （关联到云仓库）
5. git push -u origin master   （push到仓库的master仓库上）

==注意==：若远程仓库有readme.md等文件，需要先执行 git pull命令，即在git push 前，先执行

```shell
git pull --rebase origin master
```

##### 3.主要命令（第二次提交）

* git pull (拉取线上代码，一般为多人开发时使用，如果个人开发直接提交即可，命令可省略)
* git add . （添加全部文件到git 暂存区）
* git commit -m ‘注释信息’ （暂存区内容添加到本地仓库中，实际作用于添加注释信息）
* git push -u origin master （提交代码到远程仓库，如果是默认主分支，也可直接使用：git push

==慎用== ：强制推送到远程仓库。如果需要强制推送代码到远程仓库，可以在推送命令中加入-f参数，使用以下命令：

```powershell
git push -f [远程仓库名] [本地分支名]:[远程分支名]
git push -f origin master
```

##### 次要命令

* git status (放在git add之前，用于查看发生变化的文件。在使用VScode编写代码时，修改的文件也会在文件名后面出现一个暗黄色的大写:M)

#### 三、git提交本地代码到分支再合并到总分支

```shell
git init # 初始化
git add . # 将所有文件都加入到暂存区
git commit -m "第一次提交" # 本地提交更新
# 在git或github中创建空白仓库
git remote add origin 'url' # 关联到云仓库
git checkout -b tabber # 创建并切换到tabber分支（包括了git branch dev + git checkout dev两行代码）
git add .
git commit -m "完成了 tabBar 的开发"
git push -u origin tabbar # 将本地的 tabbar 分支推送到远程仓库进行保存
git checkout master # 切换到主分支
git merge tabbar # 将本地的 tabbar 分支合并到本地的 master 分支
git branch -d tabbar #  删除本地的 tabbar 分支
git checkout -b home # 切换到新的分支继续开发
```



#### 四、其他命令

##### 1.查看用户信息

```shell
用户名： $ git config user.name 
邮箱：$ git config user.email
密码：$ git config user.password  
```

##### 2.修改用户信息

```shell
用户名：$ git config --global --replace-all user.name "要修改的用户名"
邮箱： $ git config --global --replace-all user.email"要修改的邮箱"
密码：$ git config --global --replace-all user.password "要修改的密码"
```

##### 3.其他常用命令

```shell
查看修改状态：git status
拉取远程仓库代码：git pull
克隆远程某分支上的代码：git clone -b 分支名称 http://xxx.git
合并分支到主分支：git merge 分支名称
创建新分支：git branch 新分支名
删除分支：git branch -D 分支名
查看分支：git branch
分支切换：git checkout 分支名称
查看记录：git log
查看地址：git remote -v
强制合并代码（用于当前版本和历史提交版本不一致的情况）：git pull origin 分支名--allow-unrelated-histories
本地代码覆盖远程分支代码：git push -f --set-upstream origin 分支名
```

##### 4.修改远程仓库

```shell
# 查看远端地址
git remote -v  
# 查看远端仓库名
git remote 
# 重新设置远程仓库
git remote set-url origin https://gitee.com/xx/xx.git (新地址)
```

> 参考：https://blog.csdn.net/weixin_51033461/article/details/119997189