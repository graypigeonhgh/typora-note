# Spring Cache的配置与使用



>  1、SpringCache是Spring提供的一个缓存框架，在Spring3.1版本开始支持将缓存添加到现有的spring应用程序中，在4.1开始，缓存已支持JSR-107注释和更多自定义的选项。
>
> 2、Spring Cache利用了AOP，实现了基于注解的缓存功能，并且进行了合理的抽象，业务代码不用关心底层是使用了什么缓存框架，只需要简单地加一个注解，就能实现缓存功能了，做到了对代码侵入性做小。
>
> 3、由于市面上的缓存工具实在太多，SpringCache框架还提供了CacheManager接口，可以实现降低对各种缓存框架的耦合。它不是具体的缓存实现，它只提供一整套的接口和代码规范、配置、注解等，用于整合各种缓存方案，比如Caffeine、Guava Cache、Ehcache。 

参考自：https://blog.csdn.net/weixin_42972832/article/details/127822968



#### 1.导入依赖

```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <scope>compile</scope>
</dependency>
```



#### 2.在启动类中加入@EnableCaching注解

```java
@Slf4j
@SpringBootApplication
@EnableCaching
public class CacheDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(CacheDemoApplication.class,args);
        log.info("项目启动成功...");
    }
}
```



#### 3.使用自带的的缓存技术

```java
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private UserService userService;

    /**
     * CachePut：将方法返回值放入缓存
     * value：缓存的名称，每个缓存名称下面可以有多个key
     * key：缓存的key
     * #为SpringCache的一种表达式语言
     */
    @CachePut(value = "userCache",key = "#user.id")
    @PostMapping
    public User save(User user){
        userService.save(user);
        return user;
    }

    /**
     * CacheEvict：清理指定缓存
     * value：缓存的名称，每个缓存名称下面可以有多个key
     * key：缓存的key
     * #p0,#root.args[0],#id 都表示第一个参数：id
     */
    @CacheEvict(value = "userCache",key = "#p0")
    //@CacheEvict(value = "userCache",key = "#root.args[0]")
    //@CacheEvict(value = "userCache",key = "#id")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        userService.removeById(id);
    }

    //@CacheEvict(value = "userCache",key = "#p0.id")
    //@CacheEvict(value = "userCache",key = "#user.id")
    //@CacheEvict(value = "userCache",key = "#root.args[0].id")
    @CacheEvict(value = "userCache",key = "#result.id")
    @PutMapping
    public User update(User user){
        userService.updateById(user);
        return user;
    }

    /**
     * Cacheable：在方法执行前spring先查看缓存中是否有数据，如果有数据，则直接返回缓存数据；若没有数据，调用方法并将方法返回值放到缓存中
     * value：缓存的名称，每个缓存名称下面可以有多个key
     * key：缓存的key
     * condition：条件，满足条件时才缓存数据
     * unless：满足条件则不缓存
     * 解决缓存穿透的一种方式，缓存空对象：预防高并发访问不存在的数据，服务端直接把空值存到缓存中去，客户端再次访问时，		 * 直接返回空值，防止有人用不存在的key发起大量并发访问,导致数据库崩溃
     */
    @Cacheable(value = "userCache",key = "#id",unless = "#result == null")
    @GetMapping("/{id}")
    public User getById(@PathVariable Long id){
        User user = userService.getById(id);
        return user;
    }

    //动态拼接key
    @Cacheable(value = "userCache",key = "#user.id + '_' + #user.name")
    @GetMapping("/list")
    public List<User> list(User user){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(user.getId() != null,User::getId,user.getId());
        queryWrapper.eq(user.getName() != null,User::getName,user.getName());
        List<User> list = userService.list(queryWrapper);
        return list;
    }
}
```



#### 4. SpringBoot整合Redis和Cache

##### 4.1 application.yml相关配置(使用Redis缓存技术)

```java
spring:
  application:
    #应用的名称，可选
    name: ruiji_take_out
  #Redis相关配置
  redis:
    host: localhost
    port: 6379
    #password: a1234567
    database: 0   #操作的是0号数据库
    jedis:
      #Redis.连接池配置
      pool:
        max-active: 8  #最大连接数
        maX-wait: 1ms  #连接池最大阻塞等待时间
        maX-id几e: 4  #连接池中的最大空闲连接
        min-id几e: 0  #连接池中的最小空闲连接
  cache:
    redis:
      time-to-live: 1800000  #设置缓存有效期
```

##### 4.2 导入依赖

```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <version>3.1.2</version>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-cache</artifactId>
</dependency>
```

