# MySQL主从复制的配置



#### 1.前置条件

> (在Linux中)提前准备好两台服务器，分别安装Mysql并启动服务成功
> 主库Master: 192.168.88.131
> 从库slave: 192.168.88.132



#### 2.主库master配置

第一步：修改ysql数据库的配置文件/etc/my.cnf

```java
vim /etc/my.cnf
    
[mysqld]
log-bin=mysql-bin	#【必须】启用二进制日志
server-id=100		#【必须】服务器唯一ID
```

第二步：重启Mysq1服务

```java
systemctl restart mysqld
```

第三步：登录MySQL数据库，执行下面SQL(mysql8的用户创建和授权需要分开,授权不用加密码)，以实现远程登录

```java
1.创建用户：%表示所有地址（主机名）；@后面可以接具体ip地址 ，如@‘192.168.88.131’、@‘localhost’。
create user '同步账户名'@'从机账户' identified by '密码';
例：CREATE USER 'username'@'%' IDENTIFIED BY 'password';
2.授权：表示授予用户访问所有数据库的权限，启用test用户可以登录任何机器。
grant all privileges on *.* to '用户名'@'%' with grant option;
```

> with grant option：表示该用户可以将自己拥有的权限授权给别人
>
> *.* 中前面的\*号用来指定数据库名，后面的\*号用来指定表名
> to ：表示将权限赋予某个用户
>
> identified by： 指定用户的登录密码。

参考自：https://blog.csdn.net/fang0604631023/article/details/127804765

第四步：查看从数据库的状态,记录下结果中File和Position的值
```java
show master status;
```

![image-20231104113615588](D:/TyporaNote/image/image-20231104113615588.png)



#### 3.从库slave配置

第一步：修改Mysq1数据库的配置文件/etc/my.cnf

```java
vim /etc/my.cnf
[mysqld]
server-id=101	#【必须】服务器唯一ID
```

第二步：重启Mysq1服务

```java
systemctl restart mysqld
```

第三步：登录SQL数据库，执行下面SQL（需要修改主数据库中获取的master_log_file和master_log_pos的值)

```java
change master to master_host='192.168.88.131',master_user='hgh',master_password='Hgh675667%',master_log_file='mysql-bin.000001',master_log_pos=62106,GET_MASTER_PUBLIC_KEY=1;

start slave;

//如果报错： This operation cannot be performed with a running replica io thread;
stop slave;
change.....(如上)
start slave;
```

第四步：查看从数据库的状态

```java
show slave status;
或show slave status\G;
```

![屏幕截图 2023-11-04 220616](../../image/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-11-04%20220616.jpg)



#### 4.另外，如果Slave_IO_Running的值为No，则可能是主从服务器UUID一致（如果从服务器是克隆的主服务器，那么server-uuid的值肯定是一样的）

```java
// 1.修改从服务器的UUID：将新生成的UUID复制到server-uuid中
mysql> select uuid();
+--------------------------------------+
| uuid()                               |
+--------------------------------------+
| 45c984bb-7ac7-11ee-8f58-000c29e88e08 |
+--------------------------------------+
1 row in set (0.00 sec)

mysql> show variables like 'datadir';
+---------------+-----------------+
| Variable_name | Value           |
+---------------+-----------------+
| datadir       | /var/lib/mysql/ |
+---------------+-----------------+
1 row in set (0.00 sec)

mysql> ^DBye
[root@node2 ~]# vim /var/lib/mysql/auto.cnf

// 2、重启服务
service mysqld restart
// 继续执行第三步
```

#### 5.idea中yml文件配置

```java
spring:
  shardingsphere:
    datasource:
      names:
        master,slave
      # 主数据源
      master:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.88.131:3306/reggie?characterEncoding=utf-8
        username: root
        password: root
      # 从数据源
      slave:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.88.132:3306/reggie?characterEncoding=utf-8
        username: root
        password: root
    masterslave:
      # 读写分离配置
      load-balance-algorithm-type: round_robin #轮询
      # 最终的数据源名称
      name: dataSource
      # 主库数据源名称
      master-data-source-name: master
      # 从库数据源名称列表，多个逗号分隔
      slave-data-source-names: slave
    props:
      sql:
        show: true #开启SQL显示，默认false
  main:
    allow-bean-definition-overriding: true
```

