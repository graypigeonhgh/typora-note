# MyBatisPlus分页插件的使用

<br>

> 参考自官网：https://baomidou.com/pages/97710a/#%E6%94%AF%E6%8C%81%E7%9A%84%E6%95%B0%E6%8D%AE%E5%BA%93

#### 1.配置类方法

```java
@Configuration
@MapperScan("scan.your.mapper.package")
public class MybatisPlusConfig {

    /**
     * 添加分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        // 指定数据库方言为 MYSQL
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());//如果配置多个插件,切记分页最后添加
        //mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL)); 如果有多数据源可以不配具体类型 否则都建议配上具体的DbType
        return mybatisPlusInterceptor;
    }
}
```

