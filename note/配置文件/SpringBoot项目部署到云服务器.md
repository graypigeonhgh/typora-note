# SpringBoot项目部署到云服务器

运行SpringBoot项目的前提是服务器安装了对应的JDK版本以及配置了JDK环境

到官网去下载jdk的Linux版本，官网地址：https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html，如JDK17版本：https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz

> 在云服务器中安装JDK步骤：https://blog.csdn.net/Turniper/article/details/130306387?

> 查看JDK版本命令：java -version

#### 1. 将SpringBoot打包好的jar包上传到服务器中

这里演示的是上传到root/java_project目录下：

![image-20240226174759166](https://hgh-typora-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240226174759166.png)

#### 2. 重命名jar包(可选)

```sql
mv yuzhan_android-0.0.1-SNAPSHOT.jar yuzhan.jar
```

#### 3.将标准输出和错误输出重定向到一个文件中（追加到原有内容的后面）

```sql
nohup java -jar yuzhan.jar >> mylog.log 2>&1 &ls
```



**说明：**

nohup: 不挂断地运行命令。常规用法：联合使用nohup和&让进程后台运行

标准输入（stdin）：代码为0，使用<或<<;

标准输出（stdout）：代码为1，使用>或>>;

\> 覆盖写	；>> 追加写

&>或2>&1     将标准输出与错误输出共同写入到文件中

#### 4. 查看日志内容

> tail -f mylog.log

#### 5. 或者直接运行jar包(不建议)

```sql
java -jar yuzhan.jar
```

