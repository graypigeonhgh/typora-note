# Lombok的Builder注解

以前我们是使用 new 对象后，再逐行执行 set 方法的方式来给对象赋值的。

还有另外一种可能更方便的方式： builder。

1）实体类加上 @Builder 等注解：

```java
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExecuteCodeRequest {

    private List<String> inputList;

    private String code;

    private String language;
}
```

2）可以使用链式的方式更方便地给对象赋值：

```java
ExecuteCodeRequest executeCodeRequest = ExecuteCodeRequest.builder()
    .code(code)
    .language(language)
    .inputList(inputList)
    .build();
```