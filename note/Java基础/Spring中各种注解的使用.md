# Spring中各种注解的使用

> 何为注解？注解本质上就是一个类，开发中我们可以使用注解 取代 xml配置文件。

### 1. @PathVariable,@RequestBody和@RequestParam的区别

**@PathVariable**

主要用于接收http://host:port/path/{参数值}数据

@PathVariable 支持下面三种参数：

- name 绑定本次参数的名称，要跟URL上面的一样
- required 这个参数是否必须的
- value 跟name一样的作用，是name属性的一个别名

**@RequestBody**

注解@RequestBody接收的参数是来自requestBody中，即**请求体**。一般用于处理非 Content-Type: application/x-www-form-urlencoded编码格式的数据，比如：==application/json、application/xml==等类型的数据。通常用于接收POST、DELETE等类型的请求数据，GET类型也可以适用。

```java
@PostMapping("/saveBatch")
public String saveBatch(@RequestBody @Validated List<User> list){
    list.forEach(System.out::println);
    return "saveBatch success";
}
```



**@RequestParam**

接收来自RequestHeader中，即**请求头**。通常用于GET请求，像POST、DELETE等其它类型的请求也可以使用。例如：

```java
@GetMapping("/hello")
public String hello(@RequestParam(name = "id") Long id){
    System.out.println("hello " + id);
    return "hello message";
}
```

* @RequestParam用来处理`Content-Type` 为 `application/x-www-form-undencoded`编码的内容，`Content-Type` 默认为该属性

* @RequestParam也可用于其它类型的请求，例如：POST、DELETE等请求。
* 由于@RequestParam是用来处理 Content-Type 为 ==application/x-www-form-urlencoded== 编码的内容的，所以在postman中，要选择body的类型为 x-www-form-urlencoded，这样在headers中就自动变为了 Content-Type : application/x-www-form-urlencoded 编码格式
* @RequestParam 不支持直接传递实体类的方式，所以可以在实体类中做数据校验，使用 @Validated 注解使得作用在实体类属性上的注解生效

```java
@PostMapping("/save")
public String save(@Validated User user){
    System.out.println(user);
    return "hello success";
}
// console result: User(id=110, name=admin, password=123456)
```

**注意：**

* 由于GET请求没有请求体，所以在GET请求中，不能使用@RequestBody，但Post请求中,可以使用@RequestParam。
* 在POST请求，可以使用@RequestBody和@RequestParam，但是如果使用@RequestBody，对于参数转化的配置必须统一。
* 可以使用多个@RequestParam获取数据，@RequestBody不可以
* @RequestParam 和 @PathVariable 注解是用于从request中接收请求的，两个都可以接收参数，关键点不同的是@RequestParam 是从request里面拿取值，而 @PathVariable 是从一个URI模板里面来填充

> 原文链接：https://blog.csdn.net/weixin_46058921/article/details/127794325



### 2. @Component和@Bean

**@Component**

**@component（@Controller、@Service、@Repository）**是spring中的一个注解，它的作用就是实现bean的注入（把普通pojo实例化到spring容器中，相当于配置文件中的bean ）。泛指各种组件，就是说当我们的类不属于各种归类的时候（不属于@Controller、@Services等的时候），我们就可以使@Component来标注这个类。@Component注解在一个类上，表示将此类标记为Spring容器中的一个Bean。（**相当于创建对象**）

在配置文件中的书写格式,如spring mvc中的applicationcontent.xml，在spring boot中的话，因采用的是零配置所以直接在类上加入@component注解就可以了。

**@Bean**

@Bean是一个注解，用于将一个方法标记为Spring容器中的一个Bean。具体来说，**@Bean注解可以用于方法上**，该方法返回一个对象，该对象将被Spring容器管理和提供给其他程序组件使用，**让IOC容器知道这个组件存在**。（相当于**创建对象**） 

**@Autowire**

@Autowire是组件和组件相互调用的时候，自动从ioc中取出来需要用的组件。（**自动赋值，调用对象**）

比如Service，Controller，Dao的关系，这三个组件都分别加上了注册的注解：@Service，@Controller，@Component，ioc中已经有了注册信息，但是Service要用到Dao操作数据，所以在Service中的Dao头上就要用@Autowired来给Dao自动赋值，来供Service用，同理，Controller中也要用到Service，那么就要在Service上边加上@Autowired 

**区别**

* @Component（@Controller、@Service、@Repository）通常是通过类路径扫描来自动侦测以及自动装配到Spring容器中。而@Bean注解通常是我们在标有该注解的方法中定义产生这个bean的逻辑。（**@Component 作用于类，@Bean作用于方法**)

* @Component和@Bean两者的目的是一样的，都是注册bean到Spring容器中。

* @Component和@Bean都是用来注册Bean并装配到Spring容器中，但是Bean比Component的自定义性更强。可以实现一些Component实现不了的自定义加载类

例如：如果想要将第三方库中的组件装配到你的应用中，在这种情况下，是没有办法在它的类上添加@Component注解的，因此就不能使用自动化装配的方案了，但是我们可以使用@Bean,当然也可以使用XML配置。

> 原文链接：https://blog.csdn.net/u011066470/article/details/119968343



### 3. @Controller和@RestController

**@Controller**

@Controller注解 默认只能返回要跳转的路径即跳转的html/JSP页面。
只有添加了 @ResponseBody注解后才可以返回 方法中 指定的返回类型。

@Controller修饰的类会将**return的字符串**配合**视图解析器InternalResourceViewResolver**添加上前缀和后缀。跳转到对应的html页面。

**@RestController**

@RestController 注解 ，则返回的是方法指定的返回类型。

@RestController修饰的类会将 return后面的内容字符串的形式返回到@Requestmapping的页面

例如：如果返回类型为ModelAndView则返回指定的jsp页面；如果是String 则返回字符串；如果是JSONObject 则返回json对象

**区别**

* @RestController = @Controller + @ResponseBody

> 原文链接：https://blog.csdn.net/weixin_42988712/article/details/109463513?



### 4.@ControllerAdvice

>  @ControllerAdvice注解是Spring3.2中新增的注解，学名是Controller增强器，作用是给Controller控制器添加统一的操作或处理。@ControllerAdvice主要用来处理全局数据，一般搭配@[ExceptionHandler](https://so.csdn.net/so/search?q=ExceptionHandler&spm=1001.2101.3001.7020)、@ModelAttribute以及@InitBinder使用。

@ControllerAdvice是在类上声明的注解，其用法主要有三点：

1.结合方法型注解@ExceptionHandler，用于捕获Controller中抛出的指定类型的异常，从而达到不同类型的异常区别处理的目的。

2.结合方法型注解@InitBinder，用于request中自定义参数解析方式进行注册，从而达到自定义指定格式参数的目的。

3.结合方法型注解@ModelAttribute，表示其注解的方法将会在目标Controller方法执行之前执行。

```java
@ControllerAdvice
public class GlobalExceptionHandler {

    //全局异常处理，执行的方法
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        e.printStackTrace();
        return Result.fail().message("执行全局异常处理...");
    }

    //特定异常处理
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public Result error(ArithmeticException e) {
        e.printStackTrace();
        return Result.fail().message("执行特定异常处理...");
    }
}
```

> 参考自：https://blog.csdn.net/weixin_57726902/article/details/128018761c



### 5. @SneakyThrows

@SneakyThrows 是 Lombok 提供的一个注解，用于在方法上自动抛出异常。使用 @SneakyThrows 注解可以使方法在遇到异常时，自动将异常转换为 java.lang.RuntimeException 并抛出，而无需显式地在方法中编写异常处理代码。

使用 @SneakyThrows 注解时，需要注意以下几点：

@SneakyThrows 注解只能用于方法上，不能用于字段、构造函数等其他地方。
方法上使用了 @SneakyThrows 注解后，编译器会忽略该方法中的受检查异常，并自动生成异常抛出的代码。
使用 @SneakyThrows
注解时要谨慎，确保在方法中的异常处理逻辑充分而且合理。因为异常被转换为运行时异常，可能会隐藏原始的异常信息，增加调试的难度。
@SneakyThrows 注解可以配合使用多个异常类型，比如 @SneakyThrows({IOException.class,
InterruptedException.class})。
需要注意的是，Lombok 是一个Java库，用于通过注解自动消除样板代码。它可以减少代码量，提高开发效率，但在使用之前，请确保已经熟悉并理解所使用的注解的作用和影响。

> 详细原理：https://blog.csdn.net/qq_22162093/article/details/115486647?
>
> 参考：https://blog.csdn.net/weixin_50503886/article/details/132008163 

### 6. @Qualifier

@Qualifier 和 @Autowired结合使用可以通过唯一Bean的id实现自动装配，因为单独的@Autowired注解实现自动装配是按照类型优先原则的，一旦IOC容器中出现了两个类型一样的Bean，@Autowired注解就会无法辨别用那个，即而报错，但是当我们加上 @Qualifier(value = "Bean的id") 的时候就可以直接通过Bean的唯一标识（id）进行装配了。

```java
@Component
public class QualifierTest {
 
    //@Qualifier 和 @Autowired结合使用可以通过唯一Bean的id实现自动装配
    @Autowired
    @Qualifier("userA")
    private User user;
 
 
    public void test(){
        System.out.println(user.getName());
    }
}
```

> 详细说明：https://blog.csdn.net/m0_62639693/article/details/126546018
