# 序列化Serializable



#### 1.定义

> 把对象转换为字节序列的过程称为对象的序列化；
>
> 把字节序列恢复为对象的过程称为对象的反序列化。



#### 2.序列化作用

##### （1）把对象的字节序列永久地保存到硬盘上，通常存放在一个文件中

##### （2） 在网络上传送对象的字节序列

> 在很多应用中，需要对某些对象进行序列化，让它们离开内存空间，入住物理硬盘，以便长期保存。比如最常见的是Web服务器中的Session对象，当有 10万用户并发访问，就有可能出现10万个Session对象，内存可能吃不消，于是Web容器就会把一些seesion先序列化到硬盘中，等要用了，再把保存在硬盘中的对象还原到内存中。
>
> 当两个进程在进行远程通信时，彼此可以发送各种类型的数据。无论是何种类型的数据，都会以二进制序列的形式在网络上传送。发送方需要把这个Java对象转换为字节序列，才能在网络上传送；接收方则需要把字节序列再恢复为Java对象。

参考自：[一篇搞懂java序列化Serializable](https://blog.csdn.net/weter_drop/article/details/84660173)



##### （3）一个类只有实现了Serializable接口，它的对象才能被序列化

> 当我们需要把对象的状态信息通过网络进行传输，或者需要将对象的状态信息持久化，以便将来使用时都需要把对象进行序列化。
>
> 网络上传输的都是二进制，不会直接传输对象。
>
> 我在开发过程中，实体并没有实现序列化，但我同样可以将数据保存到mysql、Oracle数据库中，为什么非要序列化才能存储呢？
>
> int、long、boolean类型等，都是基本数据类型，数据库里面有与之对应的数据结构。从类声明来看，我们以为的没有进行序列化，其实是在声明的各个不同变量的时候，由具体的数据类型帮助我们实现了序列化操作。所以就算我们不实现serializable依旧可以正常操作。序列化操作用于存储时，一般是对于NoSql数据库。

参考自：[实体类实现 Serializable](https://blog.csdn.net/qq_30959903/article/details/121626723)



#### 3.应用场景

```java
@Data
public class Dish implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    //菜品名称
    private String name;
    //菜品分类id
    private Long categoryId;
    //菜品价格
    private BigDecimal price;
}
```

